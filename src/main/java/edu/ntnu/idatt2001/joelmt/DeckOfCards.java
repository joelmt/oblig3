package edu.ntnu.idatt2001.joelmt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Represents a deck of cards. A deck of cards contains 52 different cards,
 * where groups of 13 cards share the same suit, and groups of 4 cards
 * share the same face but have different suits.
 *
 * @author Joel Mattias Tømmerbakk
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> deckOfCards;
    private final char[] suit = {'S', 'H', 'D', 'C'};

    /**
     * Creates an instance of a DeckOfCards containing
     * 52 different instances of PlayingCard.
     *
     */
    public DeckOfCards() {
        this.deckOfCards = new ArrayList<>();
        for (char c : suit) {
            for (int j = 1; j <= 13; j++) {
                PlayingCard card = new PlayingCard(c, j);
                this.deckOfCards.add(card);
            }
        }
    }

    /**
     * Returns an list containing all the playing cards
     * in the deck of cards.
     *
     * @return a list containing the entire deck of cards
     */
    public ArrayList<PlayingCard> getDeckOfCards() {
        return deckOfCards;
    }

    /**
     * An inner class that represents a hand of cards.
     * The hand contains no duplicate cards, as the hand is
     * created by pulling a number of random cards from
     * the deck of cards.
     *
     */
    class HandOfCards {
        private ArrayList<PlayingCard> hand;

        /**
         * Method for creating and filling an ArrayList with a number
         * n randomly chosen cards from the deck of cards.
         *
         * @param n number of cards you want the hand to contain
         * @return a list containing the randomly picked cards
         */
        public ArrayList<PlayingCard> dealHand(int n) {
            hand = new ArrayList<>();
            Random random = new Random();
            while (hand.size() < n) {
                PlayingCard randomCard = deckOfCards.get(random.nextInt(52));
                if (!hand.contains(randomCard)) { //needs equals method
                    hand.add(randomCard);
                }
            }
            return hand;
        }

        /**
         * Method for finding the sum of all the cards in the hand
         *
         * @return the sum of all the faces of the cards in the hand
         */
        public int sumOfHand() {
            return hand.stream().map(PlayingCard::getFace).reduce((a, b) -> a + b).get();
        }

        /**
         * Returns a string containing all the playing cards with the suit of hearts (H)
         * in the hand
         *
         * @return a string containing all the cards with the suit of hearts from the hand
         */
        public String getHearts() {
            List<PlayingCard> listOfHearts = hand.stream().filter(c -> c.getSuit() == 'H').collect(Collectors.toList());
            String heartCards = "";
            for (PlayingCard playingCard : listOfHearts) {
                heartCards += playingCard.getAsString() + " ";
            }
            if (listOfHearts.isEmpty()) {
                heartCards = "No hearts";
            }
            return heartCards;
        }

        /**
         * Method for checking if the hand contains the queen of
         * spades playing card.
         *
         * @return yes if hand contains queen of spades, no otherwise
         */
        public String checkForQueenOfSpades() {
            if (hand.stream().anyMatch(c -> c.getAsString().equals("S12"))) {
                return "Yes";
            } else {
                return "No";
            }
        }

        /**
         * Method for checking if all the cards in the hand have the
         * same suit. This is called a "flush".
         *
         * @return yes if hand is a flush, no otherwise
         */
        public String checkForFlush() {
            if (hand.stream().allMatch(c -> c.getSuit() == hand.get(0).getSuit())) {
                return "Yes";
            } else {
                return "No";
            }
        }

        /**
         * Method for returning hand as an ArrayList
         *
         * @return hand as ArrayList
         */
        public ArrayList<PlayingCard> getHand() {
            return hand;
        }

        /**
         * Method for returning a String containing
         * all the cards in the hand
         *
         * @return a string containing all cards in hand
         */
        @Override
        public String toString() {
            String handString = "";
            for (PlayingCard card: hand) {
                handString += card.getAsString() + " ";
            }
            return handString;
        }
    }
}
