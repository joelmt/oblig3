package edu.ntnu.idatt2001.joelmt;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Class for the application "Card App"
 *
 * @author Joel Mattias Tømmerbakk
 */
public class CardApp extends Application {

    /**
     * Method for running the application
     *
     */
    public static void main(String[] args) {
        launch(args);
    }

    //Creates instance of DeckOfCard and HandOfCard
    DeckOfCards deckOfCards = new DeckOfCards();
    DeckOfCards.HandOfCards hand = deckOfCards.new HandOfCards();

    //Creates all the Text objects used in the application
    Text cards = new Text();
    Text sumText = new Text();
    Text heartsText = new Text();
    Text queenOfSpadesText = new Text();
    Text flushText = new Text();

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Card Game");

        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);

        //Creates buttons for dealing and checking hand and sets actions
        Button dealButton = new Button("Deal Hand");
        Button checkButton = new Button("Check Hand");
        dealButton.setOnAction(e -> deal());
        checkButton.setOnAction(e -> check());

        //Adding buttons to the gridPane
        gridPane.add(dealButton, 1, 1);
        gridPane.add(checkButton, 2, 1);

        //Sets fonts and text-content for all text objects and adds them to the gridPane
        cards.setFont(new Font(20));
        cards.setText("Click the deal button above to get your cards");
        gridPane.add(cards, 1, 3, 2, 1);

        sumText.setFont(new Font(15));
        sumText.setText("Sum of cards: ");
        gridPane.add(sumText, 1, 5, 2, 1);

        heartsText.setFont(new Font(15));
        heartsText.setText("Cards of hearts: ");
        gridPane.add(heartsText, 1, 6, 2, 1);

        queenOfSpadesText.setFont(new Font(15));
        queenOfSpadesText.setText("Queen of spades: ");
        gridPane.add(queenOfSpadesText, 1, 7, 2, 1);

        flushText.setFont(new Font(15));
        flushText.setText("Flush: ");
        gridPane.add(flushText, 1, 8, 2, 1);

        //Creates a scene and adds it to the stage
        Scene scene = new Scene(gridPane, 500, 250 );
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Method for dealing cards to hand
     * and displaying it.
     * It also resets the other text object.
     *
     */
    private void deal() {
        hand.dealHand(5);
        cards.setText(hand.toString());
        sumText.setText("Sum of cards: ");
        heartsText.setText("Cards of hearts: ");
        queenOfSpadesText.setText("Queen of spades: ");
        flushText.setText("Flush: ");
    }

    /**
     * Method for running methods for checking the hand.
     * It displays the sum of the cards, all cards of hearts,
     * and whether or not the hand contains a queen of spades and is
     * a flush
     *
     */
    private void check() {
        sumText.setText("Sum of cards: " + hand.sumOfHand());
        heartsText.setText("Cards of hearts: " + hand.getHearts());
        queenOfSpadesText.setText("Queen of spades: " + hand.checkForQueenOfSpades());
        flushText.setText("Flush: " + hand.checkForFlush());
    }
}
