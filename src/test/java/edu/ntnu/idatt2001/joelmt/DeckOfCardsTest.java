package edu.ntnu.idatt2001.joelmt;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class responsible for testing methods from the DeckOfCards class and the HandOfCards inner class
 *
 * @author joelmt
 */
class DeckOfCardsTest {


    /**
     * Methods that tests if deal hand
     * properly deal the right amount of cards
     *
     */
    @Test
    void dealHandTest() {
        DeckOfCards deckOfCards = new DeckOfCards();
        DeckOfCards.HandOfCards hand = deckOfCards.new HandOfCards();
        hand.dealHand(5);
        assertEquals(hand.getHand().size(), 5);
    }

    /**
     * Methods that tests if the sum of
     * the cards is calculated correctly
     *
     */
    @Test
    void sumOfHandTest() {
        DeckOfCards deckOfCards = new DeckOfCards();
        DeckOfCards.HandOfCards hand = deckOfCards.new HandOfCards();
        hand.dealHand(5);
        int sum = 0;
        for (PlayingCard card : hand.getHand()) {
            sum += card.getFace();
        }
        assertEquals(hand.sumOfHand(), sum);
    }

    /**
     * Methods that tests if a potential
     * queen of spades is found like intended
     *
     */
    @Test
    void checkForQueenOfSpadesTest() {
        DeckOfCards deckOfCards = new DeckOfCards();
        DeckOfCards.HandOfCards hand = deckOfCards.new HandOfCards();
        hand.dealHand(5);
        PlayingCard queenOfSpades = new PlayingCard('S', 12);
        boolean check = hand.getHand().contains(queenOfSpades);
        boolean containsQueenOfSpades = true;
        if (hand.checkForQueenOfSpades().equals("No")) {
            containsQueenOfSpades = false;
        }
        assertEquals(check, containsQueenOfSpades);
    }

    /**
     * Methods that tests if the method
     * for checking for flush gives the
     * expected output
     *
     */
    @Test
    void checkForFlushTest() {
        DeckOfCards deckOfCards = new DeckOfCards();
        DeckOfCards.HandOfCards hand = deckOfCards.new HandOfCards();
        hand.dealHand(5);
        String flush = "Yes";
        char check = hand.getHand().get(0).getSuit();
        for (PlayingCard card : hand.getHand()) {
            if (!(card.getSuit() == check)) {
                flush = "No";
            }
        }
        assertEquals(hand.checkForFlush(), flush);
    }

    /**
     * Methods that tests if the method
     * for checking for cards of hearts gives
     * the expected output
     *
     */
    @Test
    void cardsOfHeartsTest() {
        DeckOfCards deckOfCards = new DeckOfCards();
        DeckOfCards.HandOfCards hand = deckOfCards.new HandOfCards();
        hand.dealHand(5);
        String heartCards = "";
        for (PlayingCard card : hand.getHand()) {
            if (card.getSuit() == 'H') {
                heartCards += card.getAsString() + " ";
            }
        }
        if (heartCards.equals("")) {
            heartCards = "No hearts";
        }
        assertEquals(hand.getHearts(), heartCards);
    }
}